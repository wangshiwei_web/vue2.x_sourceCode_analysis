export function createElement(tag, data = {}, ...children) {
  return vnode(tag, data, data.key, children);
}

export function createTextVnode(text) {
  return vnode(undefined, undefined, undefined, undefined, text);
}
//用来产生虚拟dom
export function vnode(tag, data, key, children, text) {
  return {
    tag,
    data,
    key,
    children,
    text,
  };
}