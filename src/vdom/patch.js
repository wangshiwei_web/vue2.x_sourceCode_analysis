export function patch(oldVnode, Vnode) {
  //将虚拟节点转换成真实的节点
  let el = createELm(Vnode); // 产生一个新的DOM
  let parentElm = oldVnode.parentNode; //获取老元素（app） 父亲 ，body
  parentElm.insertBefore(el, oldVnode.nextSibling);
  parentElm.removeChild(oldVnode);
  //重新赋值
  return el;
}

function createELm(vnode) {
  let { tag, children, key, data, text } = vnode;
  if (typeof tag === "string") {
    //创建元素放到vnode.el上
    vnode.el = document.createElement(tag);
    Object.keys(data).forEach((ele) => {
      let styleArr = [];
      if (ele == "style") {
        Object.keys(data[ele]).forEach((item) => {
          styleArr.push(`${item}:${data[ele][item]};`);
        });
        vnode.el.setAttribute(ele, styleArr.join(""));
        styleArr = [];
      } else {
        vnode.el.setAttribute(ele, data[ele]);
      }
    });
    children.forEach((child) => {
      vnode.el.appendChild(createELm(child));
    });
  } else {
    //文本
    vnode.el = document.createTextNode(text);
  }
  return vnode.el;
}

//思路 ：虚拟dom 变成正式的dom 在此过程中会将data中的数据赋值给对应的字段 （触发get）
// 1获取到真实的dom  虚拟daom
// 2.将虚拟dom变成正式dom
// 3.获取到旧dom的父亲元素
// 4.将新的dom 方法 app 后面
// 5.删除 就的元素
