import Vue from './instance/index'
import {initGlobalApi} from './global-api/index'
initGlobalApi(Vue)

export default Vue
