/**
 * 重写数组方法
 */
let oldArrayProtoMethods = Array.prototype;
export let ArrayMethods = Object.create(oldArrayProtoMethods);
let methods = ["push", "pop", "unshift", "shift", "splice"];
methods.forEach((item) => {
  ArrayMethods[item] = function (...args) {
    let result = oldArrayProtoMethods[item].apply(this, args);
    let insert;
    switch (item) {
      case "push":
      case "unshift":
        insert = args;
        break;
      case "splice":
        insert = args.splice(2);
        break;
    }
    let ob = __ob__
    if(insert) ob.observeArray(insert)
    return result;
  };
});
