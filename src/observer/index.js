import  {ArrayMethods} from './arr'
import Dep from './dep';
export function observer(data) {
  // data 不是对象或者为null 不做处理直接返回 否则对对象进行响应式处理
  if (typeof data != "object" || data == null) return;
  if(data.__data__){
    return data
  }
  return new Observer(data);
}
class Observer {
  constructor(value) {
    this.dep = new Dep();
    console.log(this.dep,'kk-deps')
    Object.defineProperty(value,'__ob__',{
      enumerable:false,
      configurable:false,
      value:this
    })
    // data如果是数组，劫持数组方法达到添加响应式的目的 如果是对象，给对象建立响应式
    if(Array.isArray(value)){
      value.__proto__ = ArrayMethods
      this.observerArray(value)
    }else{
      this.walk(value);
    }
  }
  walk(data) {
    let keys = Object.keys(data);
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i];
      let value = data[key];
       // definedProperty 只能劫持单个属性 所以 data里的数据要遍历调用definedProperty方法
      defineReactive(data, key, value);
    }
  }
  observerArray(data){
    for(let i = 0;i<data.length;i++){
      observer(data[i])
    }
  }
}
function defineReactive(obj, key, val) {
  let childDep = observer(val)//深度代理：处理数据嵌套的情形
  const property = Object.getOwnPropertyDescriptor(obj, key)
  if(property && property.configurable === false){
    return 
  }
  let dep = new Dep()
  const getter = property && property.get
  const setter = property && property.set
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: true,
    get() {
      if(Dep.target){
        dep.depend()
        if(childDep) childDep.dep.depend()
      }
      const value = getter ? getter.call(data) : val
      console.log(dep,childDep,'kk-dep')
      return value;
    },
    set(newValue) {
      const value = getter ? getter.call(obj) : val
      if (newValue == value) return;
      setter ? setter.call(obj,newValue) : val = newValue
      observer(newValue)//如果用户设置的是对象，要给新设置的对象的每一个子项都建立响应式
      dep.notify()
    },
  });
}
