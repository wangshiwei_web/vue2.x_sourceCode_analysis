import { popTarget, pushTarget } from "./dep";

let id = 0;
class Watcher {
  constructor(vm,expOrfn,cb,options){
    this.vm = vm
    this.expOrfn = expOrfn
    this.cb = cb
    this.options = options
    this.id = id++
    this.deps = []
    this.depsId = new Set()
    if(typeof this.expOrfn === 'function'){
      this.getter = expOrfn
    }
    this.get()
  }
  addDep(dep){
    let id = dep.id
    if(!this.depsId.has(id)){
      this.deps.push(dep)
      this.depsId.add(id)
      dep.addSub(this)
    }
  }
  get(){
    pushTarget(this)
    this.getter()
    popTarget(this)
  }
  update(){
    this.get()
  }
}
export default Watcher