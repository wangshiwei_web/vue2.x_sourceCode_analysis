
/**
 * 思路：将ast转换为render函数
  * {
      "tag":"div",
      "type":1,
      "children":[
        {"type":3,"text":"你好啊！{{msg}}{{name}}"}
      ],
      "attrs":[
        {"name":"id","value":"app"},
        {"name":"class","value":"ceshi"},
        {"name":"style","value":{"background-color":"darkcyan","height":" 300px"}}
      ],
      "parent":null
    }
   _c('div',{id:"app",class:"ceshi",style:{"background-color":"darkcyan","height":" 300px"}},_v("你好啊！"+_s(msg)+_s(name)),_c('div',undefined,_v("测试")))
 * 
 * 
 */

const defaultTagRE = /\{\{((?:.|\r?\n)+?)\}\}/g; //注意正则匹配 lastIndex
// 处理元素的属性
function genProps(attrs) {
  //处理属性
  let str = "";
  for (let i = 0; i < attrs.length; i++) {
    let attr = attrs[i];
    //注意;   style："color:red;font-size: 20px
    if (attr.name === "style") {
      let obj = {}; //对样式进行特殊处理
      attr.value.split(";").forEach((item) => {
        let [key, value] = item.split(":");
        obj[key] = value;
      });
      attr.value = obj; //
    }
    //其他  'id:app',注意最后会多个属性化 逗号
    str += `${attr.name}:${JSON.stringify(attr.value)},`;
  }
  return `{${str.slice(0, -1)}}`; // -1为最后一个字符串的位置  演示一下
}
//判断是否有子集
function genChildren(el) {
  const children = el.children;
  if (children) {
    return children.map((child) => gen(child)).join(",");
  }
}
function gen(node) {
  if (node.type === 1) {
    return generate(node);
  } else {
    let text = node.text; // 获取文本 (你好啊！{{msg}}{{name})
    if (!defaultTagRE.test(text)) {
      return `_v(${JSON.stringify(text)})`;
    }
    let tokens = [];
    let lastIndex = (defaultTagRE.lastIndex = 0); 
    let match;
    while ((match = defaultTagRE.exec(text))) {
      let index = match.index; 
      if (index > lastIndex) {
        tokens.push(JSON.stringify(text.slice(lastIndex, index))); //添加的是文本 (你好啊！)
      }
      /**
       *  [0: "{{msg}}"
          1: "msg"
          groups: undefined
          index: 4
          input: "你好啊！{{msg}}{{name}}"]
       */
      tokens.push(`_s(${match[1].trim()})`); // "_s(msg)"
      lastIndex = index + match[0].length; //最后 {{}} 索引位置
    }
    if (lastIndex < text.length) {
      tokens.push(JSON.stringify(text.slice(lastIndex)));
    }
    /**
     * token : [0:"\"你好啊！\"",1: "_s(msg)",2: "_s(name)"]
     */
    return `_v(${tokens.join("+")})`;
  }
}
export function generate(el) {
  let children = genChildren(el);
  //方法：拼接字符串  ${el.attrs.length?`{style:{color:red}}`:'undefined'}
  let code = `_c('${el.tag}',${
    el.attrs.length ? `${genProps(el.attrs)}` : "undefined"
  }${children ? `,${children}` : ""})`;
  return code;
}
