import { parseHTML } from "./parseAst";
import {generate} from './generate.js'
/**
 * 模板编译流程：
 * 1:将html解析为ast
 * 2:将ast解析为render函数
 *  （1） ast 语法树变成 字符串  （2）字符串变成函数
 */
export function compileToFunction(template) {
  let ast = parseHTML(template);
  let code = generate(ast)
  let render = new Function(`with(this){return ${code}}`)
  return render
}

/**
 * {
    "tag":"div",
    "type":1,
    "children":[
        {
            "type":3,
            "text":"你好啊！{{msg}}{{name}}"
        }
    ],
    "attrs":[
        {
            "name":"id",
            "value":"app"
        },
        {
            "name":"class",
            "value":"ceshi"
        },
        {
            "name":"style",
            "value":{
                "background-color":"darkcyan",
                "height":" 300px"
            }
        }
    ],
    "parent":null
}
 * 
 * 
 */
