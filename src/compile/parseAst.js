const ncname = `[a-zA-Z_][\\-\\.0-9_a-zA-Z]*`; // 小a-z 大A到Z 标签名称： div  span a-aa
const qnameCapture = `((?:${ncname}\\:)?${ncname})`; // 捕获这种 <my:xx> </my:xx>
const startTagOpen = new RegExp(`^<${qnameCapture}`); // 标签开头的正则 捕获的内容是标签名
const endTag = new RegExp(`^<\\/${qnameCapture}[^>]*>`); // 匹配标签结尾的 </div>
const attribute = /^\s*([^\s"'<>\/=]+)(?:\s*(=)\s*(?:"([^"]*)"+|'([^']*)'+|([^\s"'=<>`]+)))?/; // 匹配属性的
const startTagClose = /^\s*(\/?)>/; // 匹配标签结束的  <div></div>  <br/>
const defaultTagRE = /\{\{((?:.|\r?\n)+?)\}\}/g; // {{xx}}  默认的 双大括号
//创建树
function createASTELement(tagName, attrs) {
  return {
    tag: tagName, //标签名称
    type: 1, //元素类型
    children: [], // 孩子列表
    attrs, //属性集合
    parent: null, // 父元素
  };
}
let root; //判断是否是根元素
let currentParent; //这个元素的当前父亲元素
//检测 标签是否符合预期 <div><span></span></div>   栈的方式来解决这个： [div,span]
let stack = [];
function start(tagName, attrs) {
  let element = createASTELement(tagName, attrs);
  if (!root) {
    root = element;
  }
  currentParent = element; //当前解析的标签保存起来
  stack.push(element);
}
//<div>hello<span></span> <p></p></div> // [div,span]
function end(tagName) {
  let element = stack.pop(); //取出 栈中的最后一个
  currentParent = stack[stack.length - 1];
  if (currentParent) {
    //在闭合时可以知道这个标签的父亲是谁
    element.parent = currentParent;
    currentParent.children.push(element); //将儿子放进去
  }
}
function chars(text) {
  //注意：空格
  text = text.replace(/\s/g, "");
  if (text) {
    currentParent.children.push({
      type: 3,
      text,
    });
  }
}

export function parseHTML(html) {
  /*解析标签 
   1:<div id="my">hello {{name}} <span>world</span></div>    <div id="my">hello {{name}} </div> ===> stack[0]
   2:<span>world</span>                                      <span>world</span>   ===> stack[1]
   */
  while (html) {
    // 只要html 不为空字符串就一直执行下去
    let textEnd = html.indexOf("<");
    let text;
    if (textEnd === 0) {
      const startTagMatch = parseStartTag();
      if (startTagMatch) {
        start(startTagMatch.tagName, startTagMatch.attrs);
        continue;
      }
      const endTagMatch = html.match(endTag);
      if (endTagMatch) {
        advance(endTagMatch[0].length);
        end(endTagMatch[1]);
        continue;
      }
    }
    if (textEnd > 0) {
      text = html.substring(0, textEnd);
    }
    if (text) {
      advance(text.length);
      chars(text);
    }
  }
  function advance(n) {
    html = html.substring(n);
  }
  //匹配 开头的标签
  function parseStartTag() {
    const start = html.match(startTagOpen);
    /**
     * 1：成功结果    2:false
     * [
          0: "<div"
          1: "div"
          groups: undefined
          index: 0
          input: "<div id=\"my\">hello {{name}} <span>world</span></div>"
        ]
     */
    if (start) {
      const match = {
        tagName: start[1],
        attrs: [],
      };
      /*
        删除开始标签  id="my">hello {{name}} <span>world</span></div>
      */
      advance(start[0].length);
      let end;
      let attr;
      while (
        !(end = html.match(startTagClose)) &&
        (attr = html.match(attribute))
      ) {
        match.attrs.push({
          name: attr[1],
          value: attr[3] || attr[4] || attr[5],
        });
        advance(attr[0].length);
      }
      if (end) {
        advance(end[0].length); //删除 >
        return match;
      }
    }
  }
  return root;
}
