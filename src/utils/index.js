export const HOOKS = [
  "beforeCreate",
  "created",
  "beforeMount",
  "mounted",
  "beforeUpdate",
  "updated",
  "beforeDestroy",
  "destroyed",
];
//策略模式
const starts = {};
HOOKS.forEach((hook) => {
  starts[hook] = mergeHook;
});
const defaultStart = function (parentVal, childVal) {
  return childVal === undefined ? parentVal : childVal
}
/**
 * 
 * @param {*} parentVal 已有配置
 * @param {*} childVal  新配置
 * @returns 合并后的配置
 * 合并相关配置
 */
function mergeHook(parentVal, childVal) {
  if (childVal) {
    if (parentVal) {
      return parentVal.concat(childVal);
    } else {
      return [childVal];
    }
  } else {
    return parentVal;
  }
}

/**
 *
 * @param {*} parent 已有的配置
 * @param {*} child 新的配置
 * @returns options 合并后的配置
 * @example created:[a,b,c]
 */
export function mergeOptions(parent, child) {
  const options = {};
  console.log(parent, child, "kk-mergeOptions");
  for (let key in parent) {
    mergeField(key);
  }
  for (let key in child) {
    if (!parent.hasOwnProperty(key)) {
      mergeField(key);
    }
  }
  function mergeField(key) {
    const start = starts[key] || defaultStart
    options[key] = start(parent[key], child[key]);
  }
  return options;
}
