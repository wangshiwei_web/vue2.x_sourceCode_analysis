import {initProxy} from './proxy'
import { initState } from "./state";
import {initEvents} from './events'
import {initRender} from './render'
import { compileToFunction } from "../compile/index.js";
import { callHook, mountComponent,initLifecycle } from "./lifecycle";
import { mergeOptions } from "../utils/index";
/**
 * 
 * @param {*} Vue 
 * 初始化流程：初始化状态 > 初始化编译模版
 *  初始化编译模板： render > template > el
 */
export function initMixin(Vue) {
  /**
   * 流程：
   *  1.合并配置
   *  2.render代理
   *  3.初始化生命周期,事件中心，inject state provide,调用生命周期
   */
  Vue.prototype._init = function (options) {
    let vm = this;
    vm.$options = options;
    vm.$options = mergeOptions(Vue.options, options); // 需要将用户自定义的options 合并 谁和谁合并
    
    initProxy(vm)
    initLifecycle(vm)
    initEvents(vm)
    callHook(vm, "beforeCreate");
    initState(vm); //初始化状态
    callHook(vm, "created");
    initRender(vm)
  };
  Vue.prototype.$mount = function (el) {
    const vm = this;
    const options = vm.$options; 
    el = document.querySelector(el);
    vm.$el = el;
    if (!options.render) {
      let template = options.template;
      if (!template && el) {
        template = el.outerHTML;
      }
      /* 
        1.获取html 生成render函数 
        2.将render函数挂载到Vue实例上 
        3.将render函数转化为Vnode 
        4.将Vnode转化为真实dom 
        5.将真实dom挂载
      */
      let render = compileToFunction(template);
      options.render = render;
    }
    mountComponent(vm, el);
  };
}
