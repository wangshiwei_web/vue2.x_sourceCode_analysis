import {createElement,createTextVnode,vnode} from '../vdom/vnode'

export function initRender (vm) {
  if (vm.$options.el) {
    vm.$mount(vm.$options.el)
  }
}
export function renderMixin(Vue) {
  Vue.prototype._c = function () {
    return createElement(...arguments);
  };
  Vue.prototype._s = function (val) {
    //注意值的类型
    return val == null
      ? ""
      : typeof val == "object"
      ? JSON.stringify(val)
      : val;
  };
  Vue.prototype._v = function (text) {
    return createTextVnode(text);
  };
  Vue.prototype._render = function () {
    let vm = this;
    const render = vm.$options.render;
    let vnode = render.call(this);
    return vnode;
  };
}