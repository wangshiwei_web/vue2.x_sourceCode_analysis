import Watcher from "../observer/watcher";
import { patch } from "../vdom/patch";

export function initLifecycle (vm) {
  const options = vm.$options

  // locate first non-abstract parent
  let parent = options.parent
  if (parent && !options.abstract) {
    while (parent.$options.abstract && parent.$parent) {
      parent = parent.$parent
    }
    parent.$children.push(vm)
  }

  vm.$parent = parent
  vm.$root = parent ? parent.$root : vm

  vm.$children = []
  vm.$refs = {}

  vm._watcher = null
  vm._inactive = false
  vm._isMounted = false
  vm._isDestroyed = false
  vm._isBeingDestroyed = false
}
export function mountComponent(vm, el) {
  callHook(vm,'beforeMount')
  let updateComponent = ()=>{
    vm._update(vm._render());
  }
  new Watcher(vm,updateComponent,()=>{},true)
  callHook(vm,'mounted')
}

export function lifecycleMixin(Vue) {
  Vue.prototype._update = function (vnode) {
    let vm = this;
    vm.$el = patch(vm.$el, vnode);
  };
}

export function callHook(vm,hook){
  const handlers = vm.$options[hook]
  console.log(handlers,vm,'kk-handlers')
  if(handlers){
    for(let i = 0; i<handlers.length;i++){
      handlers[i].call(vm)
    }
  }
}
