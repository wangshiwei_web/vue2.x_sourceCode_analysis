import { observer } from "../observer/index";
export function initState(vm) {
  let opts = vm.$options;
  if (opts.data) {
    //如果存在data选项?: 初始化data
    initData(vm);
  }
}

function initData(vm) {
  let data = vm.$options.data;
  // data： 对象形式 和函数形式  vm._data ：将data对象数据备份劫持
  data = vm._data = typeof data === "function" ? data.call(vm) : data;
  for (let key in data) {
    proxy(vm, "_data", key);
  }
  observer(data);
}

function proxy(vm, source, key) {
  Object.defineProperty(vm, key, {
    get() {
      return vm[source][key];
    },
    set(newValue) {
      vm[source][key] = newValue;
    },
  });
}
