import { initMixin } from "./init";
import { lifecycleMixin } from "./lifecycle";
import { renderMixin } from "./render";

function Vue(options) {
  this._init(options); //初始化配置
}
initMixin(Vue);//主要作用 在Vue实例上定义_init方法
lifecycleMixin(Vue); //混合生命周期 渲染
renderMixin(Vue); // _render

export default Vue;
