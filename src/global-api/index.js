import { mergeOptions } from "../utils/index"
export function initGlobalApi(Vue){
    Vue.options = {}
    /**
     * vue.mixin: 将配置混入到vue实例上，优先级比组件内的配置高
     */
    Vue.mixin = function(mixin){
        this.options = mergeOptions(this.options,mixin)
    }
}