import babel from 'rollup-plugin-babel'//把 ES6 代码转义成 ES5 代码
import serve from 'rollup-plugin-serve'
import commonjs from '@rollup/plugin-commonjs'//将 CommonJs 规范的模块转换为 ESM 规范
import resolve from '@rollup/plugin-node-resolve'//与@rollup/plugin-commonjs 插件一起使用，配合以后就可以使用 node_modules 下的第三方模块代码了
export default {
    input: './src/index.js',
    // output:{
    //     format: 'umd',
    //     name: 'Vue',
    //     file:'./dist/vue.js',
    //     sourceMap:true
    // },
    output: [
        { file: 'dist/vue.js', format: 'umd', name: 'Vue' },
        { file: 'dist/vue.common.js', format: 'cjs', name: 'Vue', exports: 'auto' },
        { file: 'dist/vue.esm.js', format: 'es', name: 'Vue' },
      ],
    plugins:[
        babel({
            exclude:'node_modules/*'
        }),
        commonjs(),
        resolve(),
        serve({ // 3000
            port:3000,
            contentBase:'', // "" 当前目录
            openPage:'/index.html'
       })
    ]
}